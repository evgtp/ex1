let arr = [-2, 2, 1, 1, -3, 5, 0, 0, -5, 2]

function evenNum() {
  let evenArr = arr.filter((el) => el % 2 === 0)
  return evenArr
}

console.log(evenNum(arr))
